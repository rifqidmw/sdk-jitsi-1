import io from 'socket.io-client';

  // export the function to connect and use socket IO:
  
  export var socket = io('https://bridge.palapabeta.com/', {
    transports: ['websocket', 'polling'],
    });

  export const startSocketIO = () => {
    socket.connect();

    console.log('calling socket is successfull');
    socket.on('connect', () => { 
      console.log('socket react connected to server'); 
    }); 
    socket.on('disconnect', () => { 
        console.log('socket react disconnect to server'); 
    }); 
    socket.on('reconnect', () => { 
        console.log('socket react reconnect to server'); 
    }); 

    socket.on('bridge', msg => {
      console.log('getting message from socket: '+ msg)
    });
  };