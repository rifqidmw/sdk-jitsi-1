// @flow

import React, { useState } from "react";
import { NativeModules, SafeAreaView, StatusBar, StyleSheet, View, FlatList, TouchableOpacity, Text, Button, Dimensions, TextInput, Keyboard, Image, Animated, KeyboardAvoidingView} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import { ColorSchemeRegistry } from '../../../base/color-scheme';
import { List, ListItem } from 'react-native-elements'
import { appNavigate } from '../../../app/actions';
import { PIP_ENABLED, getFeatureFlag } from '../../../base/flags';
import { getConferenceName } from '../../../base/conference';
import { Container, LoadingIndicator, TintedView } from '../../../base/react';
import { connect } from '../../../base/redux';
import { ASPECT_RATIO_NARROW } from '../../../base/responsive-ui/constants';
import { TestConnectionInfo } from '../../../base/testing';
import { ConferenceNotification, isCalendarEnabled } from '../../../calendar-sync';
import MaterialTabs from 'react-native-material-tabs';
import { Chat } from '../../../chat';
import { DisplayNameLabel } from '../../../display-name';
import { SharedDocument } from '../../../etherpad';
import {
    FILMSTRIP_SIZE,
    Filmstrip,
    isFilmstripVisible,
    TileView
} from '../../../filmstrip';
import { ParticipantView, getAllParticipants, PARTICIPANT_ROLE } from '../../../base/participants';
import { AddPeopleDialog, CalleeInfoContainer } from '../../../invite';
import { LargeVideo } from '../../../large-video';
import { KnockingParticipantList } from '../../../lobby';
import { BackButtonRegistry } from '../../../mobile/back-button';
import { Captions } from '../../../subtitles';
import { isToolboxVisible, setToolboxVisible, Toolbox } from '../../../toolbox';
import {
    AbstractConference,
    abstractMapStateToProps
} from '../AbstractConference';
import { getTrackByMediaTypeAndParticipant } from '../../../base/tracks';
import { MEDIA_TYPE } from '../../../base/media';
import type { AbstractProps } from '../AbstractConference';
import SegmentedControlTab from "react-native-segmented-control-tab";
import axios from 'axios';
import Labels from './Labels';
import LonelyMeetingExperience from './LonelyMeetingExperience';
import NavigationBar from './NavigationBar';
import SwipeUpDown from 'react-native-swipe-up-down';
import {SwipeablePanel} from 'rn-swipeable-panel';
import SvgUri from 'react-native-svg-uri';
import HangupButton from '../../../toolbox/components/HangupButton';
import RaiseHandButton from '../../../toolbox/components/native/RaiseHandButton';
import VideoMuteButton from '../../../toolbox/components/VideoMuteButton';
import AudioMuteButton from '../../../toolbox/components/AudioMuteButton';
import TileViewButton from '../../../video-layout/components/TileViewButton'
import {startSocketIO, socket} from '../../../base/socketIO'
// import io from 'socket.io-client';
import { NewChat } from '../../../chat/components/native/NewChat';
import { GiftedChat } from 'react-native-gifted-chat';
import { Icon, IconChatSend } from '../../../base/icons';
import SlidingUpPanel from 'rn-sliding-up-panel';
import { muteLocal } from '../../../remote-video-menu/actions';

import styles, { NAVBAR_GRADIENT_COLORS } from './styles';

/**
 * The type of the React {@code Component} props of {@link Conference}.
 */

var width = Dimensions.get('window').width;
var height = Dimensions.get('window').height;


type Props = AbstractProps & {

    /**
     * Application's aspect ratio.
     */
    _aspectRatio: Symbol,

    /**
     * Wherther the calendar feature is enabled or not.
     */
    _calendarEnabled: boolean,

    /**
     * The indicator which determines that we are still connecting to the
     * conference which includes establishing the XMPP connection and then
     * joining the room. If truthy, then an activity/loading indicator will be
     * rendered.
     */
    _connecting: boolean,

    /**
     * Set to {@code true} when the filmstrip is currently visible.
     */
    _filmstripVisible: boolean,

    _allParticipants: Array,

    _conferenceName: string,

    _userName: string,

    _userNameNoSpace: string,

    /**
     * The ID of the participant currently on stage (if any)
     */
    _largeVideoParticipantId: string,

    /**
     * Whether Picture-in-Picture is enabled.
     */
    _pictureInPictureEnabled: boolean,

    /**
     * The indicator which determines whether the UI is reduced (to accommodate
     * smaller display areas).
     */
    _reducedUI: boolean,

    _isModerator: boolean,

    _audioMuted: boolean,

    /**
     * The indicator which determines whether the Toolbox is visible.
     */
    _toolboxStyle: StyleType,
    _toolboxVisible: boolean,

    /**
     * The redux {@code dispatch} function.
     */
    dispatch: Function
};

/**
 * The conference page of the mobile (i.e. React Native) application.
 */


class Conference extends AbstractConference<Props, *> {
    /**
     * Initializes a new Conference instance.
     *
     * @param {Object} props - The read-only properties with which the new
     * instance is to be initialized.
     */
    constructor(props) {
        super(props);
        // Bind event handlers so they are only bound once per instance.
        this._onClick = this._onClick.bind(this);
        this._onHardwareBackPress = this._onHardwareBackPress.bind(this);
        this._setToolboxVisible = this._setToolboxVisible.bind(this);
        this.onSend = this.onSend.bind(this);
        this.onReceivedBridge = this.onReceivedBridge.bind(this);
        this.getParticipantId = this.getParticipantId.bind(this);
        this._renderNewChat = this._renderNewChat.bind(this);
        this._renderMessageItem = this._renderMessageItem.bind(this);
        this._onChangeText = this._onChangeText.bind(this);
        this.state = {
            selectedIndex: 0,
            loading: false,
            axiosData: null,
            dataSource: [],
            getData: false,
            isBottomsheetActive: false,
            dataMessage: [],
            message: '',
            showSend: false,
            keyboardOffset: 0,
            _participantId: ''
        };
    }

    /**
     * Implements {@link Component#componentDidMount()}. Invoked immediately
     * after this component is mounted.
     *
     * @inheritdoc
     * @returns {void}
     */
    componentDidMount() {
        BackButtonRegistry.addListener(this._onHardwareBackPress);
        
        this._panel.show(130);

        this.getParticipantId();

        this.goForFetch();

        startSocketIO();

        this.onHandleRoomSocket();

        this.keyboardDidShowListener = Keyboard.addListener(
            'keyboardDidShow',
            this._keyboardDidShow,
        );
        this.keyboardDidHideListener = Keyboard.addListener(
            'keyboardDidHide',
            this._keyboardDidHide,
        );
    }

    componentWillUnmount() {
        BackButtonRegistry.removeListener(this._onHardwareBackPress);

        this.keyboardDidShowListener.remove();
        this.keyboardDidHideListener.remove();
    }


    _keyboardDidShow(event) {
        this.setState({
            keyboardOffset: event.endCoordinates.height,
        })
    }

    _keyboardDidHide() {
        this.setState({
            keyboardOffset: 0,
        })
    }

    /**
     * Implements React's {@link Component#render()}.
     *
     * @inheritdoc
     * @returns {ReactElement}
     */
    render() {
        return (
            <Container style = { styles.conference }>
                <StatusBar
                    barStyle = 'light-content'
                    hidden = { true }
                    translucent = { true } />
                { this._renderContent() }
            </Container>
        );
    }

    /**
     * Changes the value of the toolboxVisible state, thus allowing us to switch
     * between Toolbox and Filmstrip and change their visibility.
     *
     * @private
     * @returns {void}
     */
    _onClick() {
        this._setToolboxVisible(!this.props._toolboxVisible);
        this.closePanel();
    }

    _onHardwareBackPress: () => boolean;

    /**
     * Handles a hardware button press for back navigation. Enters Picture-in-Picture mode
     * (if supported) or leaves the associated {@code Conference} otherwise.
     *
     * @returns {boolean} Exiting the app is undesired, so {@code true} is always returned.
     */
    _onHardwareBackPress() {
        let p;

        if (this.props._pictureInPictureEnabled) {
            const { PictureInPicture } = NativeModules;

            // p = PictureInPicture.enterPictureInPicture();
        } else {
            p = Promise.reject(new Error('PiP not enabled'));
        }

        p.catch(() => {
            this.props.dispatch(appNavigate(undefined));
        });

        return true;
    }

    /**
     * Renders JitsiModals that are supposed to be on the conference screen.
     *
     * @returns {Array<ReactElement>}
     */
    _renderConferenceModals() {
        return [
            <AddPeopleDialog key = 'addPeopleDialog' />,
            // <Chat key = 'chat' />,
            <SharedDocument key = 'sharedDocument' />
        ];
    }

    /**
     * Renders the conference notification badge if the feature is enabled.
     *
     * @private
     * @returns {React$Node}
     */
    _renderConferenceNotification() {
        const { _calendarEnabled, _reducedUI } = this.props;

        return (
            _calendarEnabled && !_reducedUI
                ? <ConferenceNotification />
                : undefined);
    }

    handleIndexChange = index => {
        this.setState({
          ...this.state,
          selectedIndex: index
        });
      };

    _renderTabView(){
        return (
            <View>
              <SegmentedControlTab
                values={["Chat", "Siswa"]}
                selectedIndex={this.state.selectedIndex}
                onTabPress={this.handleIndexChange}
                tabStyle={{ backgroundColor: '#F2F2F2', borderWidth: 0, borderColor: 'transparent' }}
              />
            </View>
          );
    }

    /**
     * Renders the content for the Conference container.
     *
     * @private
     * @returns {React$Element}
     */
    _renderContent() {
        const {
            _aspectRatio,
            _connecting,
            _filmstripVisible,
            _largeVideoParticipantId,
            _reducedUI,
            _shouldDisplayTileView,
            _isModerator,
            _toolboxVisible,
            _toolboxStyle,
            _userName,
            _conferenceName,
            _audioMuted
        } = this.props;
        const { buttonStyles, buttonStylesBorderless, hangupButtonStyles, toggledButtonStyles } = _toolboxStyle;
        const showGradient = _toolboxVisible;
        const applyGradientStretching
            = _filmstripVisible && _aspectRatio === ASPECT_RATIO_NARROW && !_shouldDisplayTileView;


        if (_reducedUI) {
            return this._renderContentForReducedUi();
        }

        if (!this.getData){
            this.getData = true;
            
        }

        return (
            <>
                {
                    _shouldDisplayTileView
                        ? <TileView onClick = { this._onClick } />
                        : <LargeVideo 
                            onClick = { this._onClick } 
                            _participantId = {_participantId}/>
                }

                {
                    <CalleeInfoContainer />
                }

                {
                    _connecting
                        && <TintedView>
                            <LoadingIndicator />
                        </TintedView>
                }

                <SafeAreaView
                    pointerEvents = 'box-none'
                    style = { styles.toolboxAndFilmstripContainer }>

                    { showGradient && <LinearGradient
                        colors = { NAVBAR_GRADIENT_COLORS }
                        end = {{
                            x: 0.0,
                            y: 0.0
                        }}
                        pointerEvents = 'none'
                        start = {{
                            x: 0.0,
                            y: 1.0
                        }}
                        style = { [
                            styles.bottomGradient,
                            applyGradientStretching ? styles.gradientStretchBottom : undefined
                        ] } />}

                    <Labels />

                    <Captions onPress = { this._onClick } />
                    {/*
                      * The Toolbox is in a stacking layer below the Filmstrip.
                      */}
                    

                    <SlidingUpPanel
                        showBackdrop= {false}
                        ref={c => this._panel = c}
                        visible={true}
                        allowDragging={false}>
                                <KeyboardAvoidingView
                                    style={{
                                        backgroundColor : 'transparent'
                                    }}>
                                    <View
                                        style = {{
                                            marginBottom: 20
                                        }}>
                                            { _shouldDisplayTileView || <DisplayNameLabel participantId = { _largeVideoParticipantId } /> }

                                            <LonelyMeetingExperience />
                                            { _isModerator ? this._renderToolboxModerator() : this._renderToolboxParticipant()}
                                            {
                                                _shouldDisplayTileView ? undefined : _isModerator ? <Filmstrip /> : undefined
                                            }
                                    </View>
                                    <View
                                        style={{
                                        backgroundColor: 'white',
                                        borderTopRightRadius: 40,
                                        borderTopLeftRadius: 40,
                                        height: 20
                                        }}/>
                                    <View
                                        style={{
                                            backgroundColor : 'white'
                                        }}>
                                        <SegmentedControlTab
                                            values={_isModerator ? ["Chat", "Siswa"] : ["Chat"]}
                                            selectedIndex={this.state.selectedIndex}
                                            onTabPress={this.handleIndexChange}
                                            borderRadius={0}
                                            tabsContainerStyle={{ height: 50, backgroundColor: '#F2F2F2' }}
                                            tabStyle={{ backgroundColor: '#F2F2F2', borderWidth: 0, borderColor: 'transparent' }}
                                            activeTabStyle={{ backgroundColor: 'white', marginTop: 2 }}
                                            tabTextStyle={{ color: '#444444', fontWeight: 'bold' }}
                                            activeTabTextStyle={{ color: '#5353CB' }}
                                        />
                                        {this.state.selectedIndex == 0 ? this._renderNewChat(): this._renderParticipantView() }
                                    </View>
                                </KeyboardAvoidingView>
                    </SlidingUpPanel>
                        
                </SafeAreaView>

                <SafeAreaView
                    pointerEvents = 'box-none'
                    style = { styles.navBarSafeView }>
                    <NavigationBar />
                    { this._renderNotificationsContainer() }
                    <KnockingParticipantList />
                </SafeAreaView>

                <TestConnectionInfo />

                { _audioMuted ? this.sendStatusAudio(true) : this.sendStatusAudio(false)}
                
                { this._renderConferenceNotification() }

                { this._renderConferenceModals() }
            </>
        );
    }

    _renderToolboxParticipant(){
        const {
            _toolboxStyle
        } = this.props;
        const { buttonStyles, buttonStylesBorderless, hangupButtonStyles, toggledButtonStyles } = _toolboxStyle;
        return(
            <View
                accessibilityRole = 'toolbar'
                pointerEvents = 'box-none'
                style = { {
                    alignItems: 'flex-start',
                    flexDirection: 'row',
                    flexGrow: 0,
                    justifyContent: 'space-between',
                    paddingHorizontal: 10
                    } }>
                    <RaiseHandButton />
                     <HangupButton/>
                        <TouchableOpacity onPress={this.state.isBottomsheetActive ? this.closePanel : this.openPanel}> 
                            <View>
                                <SvgUri
                                    width="35"
                                    height="35"
                                    source={this.state.isBottomsheetActive ? {uri:'https://svgshare.com/i/Q68.svg'} : {uri:'https://svgshare.com/i/Q7p.svg'}}
                                />
                    </View>
                </TouchableOpacity>
            </View>
        )
    }

    _renderToolboxModerator(){
        const {
            _toolboxStyle
        } = this.props;
        const { buttonStyles, buttonStylesBorderless, hangupButtonStyles, toggledButtonStyles } = _toolboxStyle;
        return(
            <View
                accessibilityRole = 'toolbar'
                pointerEvents = 'box-none'
                style = { {
                    alignItems: 'flex-start',
                    flexDirection: 'row',
                    flexGrow: 0,
                    justifyContent: 'space-between',
                    paddingHorizontal: 10
                    } }>
                    <TileViewButton/>

                     <View
                        accessibilityRole = 'toolbar'
                        pointerEvents = 'box-none'
                        style = { {
                         alignItems: 'center',
                         flexDirection: 'row',
                         justifyContent: 'space-around',
                         paddingHorizontal: 10
                         } }>
                        <View
                            style = {{
                                marginHorizontal: 8
                            }}>
                                <AudioMuteButton/>
                        </View>
                        <View
                            style = {{
                                marginHorizontal: 8
                            }}>
                                <HangupButton/>
                        </View>
                        <View
                            style = {{
                                marginHorizontal: 8
                            }}>
                                <VideoMuteButton/>
                        </View>
                     </View>
                        <TouchableOpacity onPress={this.state.isBottomsheetActive ? this.closePanel : this.openPanel}> 
                            <View>
                                <SvgUri
                                    width="35"
                                    height="35"
                                    source={this.state.isBottomsheetActive ? {uri:'https://svgshare.com/i/Q68.svg'} : {uri:'https://svgshare.com/i/Q7p.svg'}}
                                />
                    </View>
                </TouchableOpacity>
            </View>
        )
    }


    _renderNewChat(){
        return(
            <SafeAreaView
                style = {{
                    width:width,
                    height:220,
                    flexDirection: 'column',
                    backgroundColor: 'white'
                }}>
                <FlatList
					style={styles.flatListWrapper}
					data={this.state.dataMessage.reverse()}
					renderItem={({ item }) => (
                        <View style={item.from === this.props._userName ? styles.containerMyMessageStyle : styles.containerTheirMessageStyle}>
                            {
                                item.from != this.props._userName ?  <Image 
                                source={{ uri: 'https://raw.githubusercontent.com/AboutReact/sampleresource/master/old_logo.png'}} 
                                style={{
                                    width: 36, 
                                    height: 36, 
                                    borderRadius: 36/ 2,
                                    alignSelf: 'center',
                                    alignItems: 'center'}} 
                                /> : undefined
                            }
                            
                            <View
                                style = {{
                                    flexDirection: 'column',
                                }}>
                                <Text
                                    style={{
                                        alignSelf: 'flex-end',
                                        marginHorizontal: 8
                                    }}>
                                    {item.from}
                                </Text>
                                <View style={item.from === this.props._userName ? styles.myMessageStyle : styles.theirMessageStyle}>
                                    <Text>{item.data}</Text>
                                </View>
                            </View>

                            {
                               item.from === this.props._userName ? <Image 
                               source={{ uri: 'https://raw.githubusercontent.com/AboutReact/sampleresource/master/old_logo.png'}} 
                               style={{
                                   width: 36,
                                   height: 36, 
                                   borderRadius: 36/ 2,
                                   alignSelf: 'center',
                                   alignItems: 'center'}} 
                               />  : undefined
                            }
                        </View>
                    )}
					keyExtractor={(item, index) => (`message-${index}`)}
				/>
                <View
                    style = {{
                        alignItems: 'center',
                        borderTopColor: 'rgb(209, 219, 231)',
                        borderTopWidth: 1,
                        flexDirection: 'row',
                        paddingHorizontal: 10
                    }}>
                    <TextInput
                        blurOnSubmit = { false }
                        multiline = { false }
                        onBlur = { false}
                        onChangeText = { this._onChangeText }
                        onFocus = { true }
                        onSubmitEditing = { this.onSend }
                        placeholder = 'Enter message here...'
                        returnKeyType = 'send'
                        ref={input => { this.textInput = input }}
                        style = { styles.inputField }/>
                    {
                        this.state.showSend ? <TouchableOpacity
                        onPress = { this.onSend }>
                        <Icon
                            src = { IconChatSend }
                            style = { styles.sendButtonIcon } />
                        </TouchableOpacity> : undefined
                    }
                </View>
            </SafeAreaView>
        )
    }

    _renderMessageItem(item){
        console.log('grom render message ', item);
        <View style={item.from === this.props._userName ? styles.myMessageStyle : styles.theirMessageStyle}>
          <Text>{item.data}</Text>
        </View>
    }

    _renderChat(){
        return (
            <>
                <Chat key = 'chat' />
            </>
        );
    }

    goForFetch = () => {
        fetch("https://jsonplaceholder.typicode.com/users")
            .then(response => response.json())
            .then((responseJson) => {
                console.log('getting data from fetch', responseJson)
                this.setState({
                    dataSource: responseJson
                })
            })
            .catch(error => console.log(error))
    }

    goForAxios = () => {
        axios.get("https://jsonplaceholder.typicode.com/users")
            .then(response => {
                this.setState({
                    axiosData: response.data,
                    dataSource: response.data
                })
            })
            .catch(error => {
                console.log(error);
            });
    }
    FlatListSeparator = () => {
        return (
            <View style={{
                height: .5,
                width: "100%",
                backgroundColor: "rgba(0,0,0,0.5)",
            }}
            />
        );
    }
    keyExtractor = (item, index) => index.toString()
    renderItem = ({data}) => {
        // <ListItem
        //     title={data.item.name}
        // />
    }

    _renderParticipantView(){        
        const { axiosData, dataSource } = this.state
        console.log('getting data from source', dataSource);
        // console.log('getting data from axios', axiosData[0]);
        return (
            <View
                style = {{
                    width:width,
                    height:220,
                    flexDirection: 'column',
                    backgroundColor: 'white'
                }}>
                 <FlatList
                        style={styles.flatListWrapper}
                        data={this.state.dataSource}
                        renderItem={({ item }) => (
                            <ListItem
                                title={`${item.name} ${item.username}`}
                                subtitle={item.email}
                            />
                            )}
                    />
            </View>
        );
    }

   getParticipantId(){
        const {
            _allParticipant,
        } = this.props;
        for (const participant of _allParticipant) {
            if (participant.role === PARTICIPANT_ROLE.MODERATOR){
                this.setState({
                    _participantId : participant.id
                })
            }
        }
   }

    startSocket(){
        this.socket = io('https://bridge.palapabeta.com/', {      
            transports: ['websocket', 'polling']});   
            this.socket.connect();

        // this.onHandleMessageSocket();
    }

    onHandleRoomSocket(){
        const jsonRoom = {
            'room': this.props._conferenceName,
          };

        socket.emit('room', jsonRoom);
        var jsonBridge = {
            'type': 'cekRome',
            'room': this.props._conferenceName,
          };
        
        socket.emit('bridge', jsonBridge);
        console.log('send socket bridge: '+ jsonBridge.toString())

        socket.on('bridge', this.onReceivedBridge); 
    }

    onReceivedBridge(data) {
        const {
            _userNameNoSpace,
            _audioMuted,
            _allParticipant,
        } = this.props;
        if (data['type'] === 'chat'){
            console.log('response for data: ', data);
            this.setState(prevState => ({
                dataMessage: [...prevState.dataMessage, data]
            }));

            console.log('data chat', this.state.dataMessage);
        } else if (data['type'] === 'mute'){
            console.log('response for mute: ', data);
            if (data['to'] === _userNameNoSpace){
                { _audioMuted ? this._setAudioMuted(false) : this._setAudioMuted(true)}
            }
        } else if (data['type'] === 'presenter'){
            console.log('response for presenter: ', data);
            if (data['status'] === false){
                this.getParticipantId();
            } else {
                for (const participant of _allParticipant) {
                    if (participant.name === data['to']){
                        this.setState({
                            _participantId : participant.id
                        })
                        console.log('presenter id: '+ this.state._participantId + ', with name: '+ participant.name);
                    }
                }
            }
        }
    }

    onSend(){
        console.log('button send is clicked');
      var objMessage = {
          'type':'chat',
          'from':this.props._userName,
          'avatar':'',
          'room': this.props._conferenceName,
          'data': this.state.message
      };

      socket.emit('bridge', objMessage);

      this.setState({
         message: '',
         showSend: false
      });

      this.textInput.clear();

      console.log('after send message, isi messaage: '+this.state.message);
    }

    sendStatusAudio(status: boolean){
        const {
            _userNameNoSpace,
            _conferenceName
        } = this.props;
        const jsonData = {
            'type':'audioStatus',
            'user':_userNameNoSpace,
            'status':status,
            'room':_conferenceName
        };

        socket.emit('bridge', jsonData);

        console.log('send status audio', jsonData);
    }

    _setAudioMuted(audioMuted: boolean) {
        this.props.dispatch(muteLocal(audioMuted));
    }


    openPanel = () => {
        console.log('bottom sheet open')
        this._setAudioMuted(true);
        this._panel.show(420);
        this.setState({ 
            isBottomsheetActive: true
        });
    };

    closePanel = () => {
        console.log('bottom sheet close')
        this._setAudioMuted(false);
        this._panel.show(130);
        this.setState({ 
            isBottomsheetActive: false
         });
    };

    _onChangeText(text) {
        this.setState({
            message: text,
            showSend: true
        });

        console.log('onchange text: '+ this.state.message);
    }

    /**
     * Renders the content for the Conference container when in "reduced UI" mode.
     *
     * @private
     * @returns {React$Element}
     */
    _renderContentForReducedUi() {
        const { _connecting } = this.props;

        return (
            <>
                <LargeVideo onClick = { this._onClick } />

                {
                    _connecting
                        && <TintedView>
                            <LoadingIndicator />
                        </TintedView>
                }
            </>
        );
    }

    /**
     * Renders a container for notifications to be displayed by the
     * base/notifications feature.
     *
     * @private
     * @returns {React$Element}
     */
    _renderNotificationsContainer() {
        const notificationsStyle = {};

        // In the landscape mode (wide) there's problem with notifications being
        // shadowed by the filmstrip rendered on the right. This makes the "x"
        // button not clickable. In order to avoid that a margin of the
        // filmstrip's size is added to the right.
        //
        // Pawel: after many attempts I failed to make notifications adjust to
        // their contents width because of column and rows being used in the
        // flex layout. The only option that seemed to limit the notification's
        // size was explicit 'width' value which is not better than the margin
        // added here.
        const { _aspectRatio, _filmstripVisible } = this.props;

        if (_filmstripVisible && _aspectRatio !== ASPECT_RATIO_NARROW) {
            notificationsStyle.marginRight = FILMSTRIP_SIZE;
        }

        return super.renderNotificationsContainer(
            {
                style: notificationsStyle
            }
        );
    }

    _setToolboxVisible: (boolean) => void;

    /**
     * Dispatches an action changing the visibility of the {@link Toolbox}.
     *
     * @private
     * @param {boolean} visible - Pass {@code true} to show the
     * {@code Toolbox} or {@code false} to hide it.
     * @returns {void}
     */
    _setToolboxVisible(visible) {
        this.props.dispatch(setToolboxVisible(visible));
    }
    
}


export const plusSlides = ()=>{
    console.log('slideIndex');
}
/**
 * Maps (parts of) the redux state to the associated {@code Conference}'s props.
 *
 * @param {Object} state - The redux state.
 * @private
 * @returns {Props}
 */
function _mapStateToProps(state) {
    const { connecting, connection } = state['features/base/connection'];
    const {
        conference,
        joining,
        membersOnly,
        leaving
    } = state['features/base/conference'];
    const { aspectRatio, reducedUI } = state['features/base/responsive-ui'];

    // XXX There is a window of time between the successful establishment of the
    // XMPP connection and the subsequent commencement of joining the MUC during
    // which the app does not appear to be doing anything according to the redux
    // state. In order to not toggle the _connecting props during the window of
    // time in question, define _connecting as follows:
    // - the XMPP connection is connecting, or
    // - the XMPP connection is connected and the conference is joining, or
    // - the XMPP connection is connected and we have no conference yet, nor we
    //   are leaving one.
    const connecting_
        = connecting || (connection && (!membersOnly && (joining || (!conference && !leaving))));
    const allParticipant = getAllParticipants(state);
    const participantId = state['features/large-video'].participantId;

    let isModerator = false;
    let userName = '';
    let userNameNoSpace = ''

    for (const participant of allParticipant) {
        if (participant.local === true) {
            userName = participant.name;
            userNameNoSpace = participant.name.split(' ').join('').replace(/[\.,-\/#!$%\^&\*;:{}=\-_`~()]/gi, '');
            if (participant.role === PARTICIPANT_ROLE.MODERATOR) {
                isModerator = true;
            } else {
                isModerator = false;
            }
        }
    }

    const tracks = state['features/base/tracks'];
    const audioTrack = getTrackByMediaTypeAndParticipant(tracks, MEDIA_TYPE.AUDIO, userId);

    var conferenceNameSplit = getConferenceName(state).split(' ');
    var conferenceNameJoin = conferenceNameSplit[2] + ' ' + conferenceNameSplit[1] + '-' + conferenceNameSplit[0] + ' ' + conferenceNameSplit[1] + '-' + conferenceNameSplit[4];
    var conferenceNameUpper = conferenceNameJoin.toUpperCase(); 

    return {
        ...abstractMapStateToProps(state),
        _aspectRatio: aspectRatio,
        _conferenceName: conferenceNameUpper,
        _calendarEnabled: isCalendarEnabled(state),
        _connecting: Boolean(connecting_),
        _filmstripVisible: isFilmstripVisible(state),
        _allParticipants: allParticipant,
        _isModerator: isModerator,
        _userName: userName,
        _userNameNoSpace: userNameNoSpace,
        _largeVideoParticipantId: state['features/large-video'].participantId,
        _pictureInPictureEnabled: getFeatureFlag(state, PIP_ENABLED),
        _reducedUI: reducedUI,
        _toolboxStyle: ColorSchemeRegistry.get(state, 'Toolbox'),
        _toolboxVisible: isToolboxVisible(state),
        _audioMuted: audioTrack?.muted ?? true,
    };
}
export default connect(_mapStateToProps)(Conference);
