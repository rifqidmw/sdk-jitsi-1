// @flow

import React, { Component } from 'react';
import { Text, View, TouchableOpacity } from 'react-native';

import { Avatar } from '../../../base/avatar';
import { ColorSchemeRegistry } from '../../../base/color-scheme';
import { BottomSheet, isDialogOpen } from '../../../base/dialog';
import { getParticipantDisplayName } from '../../../base/participants';
import { connect } from '../../../base/redux';
import { StyleType } from '../../../base/styles';
import { IconAudioOffNew } from '../../../base/icons';
import { PrivateMessageButton } from '../../../chat';
import { hideRemoteVideoMenu, muteRemote, unMuteRemote } from '../../actions';
import { getTrackByMediaTypeAndParticipant } from '../../../base/tracks';
import { MEDIA_TYPE } from '../../../base/media';
import { getConferenceName } from '../../../base/conference';
import {socket, startSocketIO} from '../../../base/socketIO'


import KickButton from './KickButton';
import MuteButton from './MuteButton';
import PinButton from './PinButton';
import styles from './styles';

import SvgUri from 'react-native-svg-uri';

/**
 * Size of the rendered avatar in the menu.
 */
const AVATAR_SIZE = 35;

type Props = {

    /**
     * The Redux dispatch function.
     */
    dispatch: Function,

    /**
     * The participant for which this menu opened for.
     */
    participant: Object,

    /**
     * The color-schemed stylesheet of the BottomSheet.
     */
    _bottomSheetStyles: StyleType,

    /**
     * Whether or not to display the kick button.
     */
    _disableKick: boolean,

    /**
     * Whether or not to display the remote mute buttons.
     */
    _disableRemoteMute: boolean,

    /**
     * True if the menu is currently open, false otherwise.
     */
    _isOpen: boolean,

    /**
     * Display name of the participant retreived from Redux.
     */
    _participantDisplayName: string,

    _audioMuted: boolean,

    _conferenceName: string,
}

// eslint-disable-next-line prefer-const
let RemoteVideoMenu_;

/**
 * Class to implement a popup menu that opens upon long pressing a thumbnail.
 */
class RemoteVideoMenu extends Component<Props> {
    /**
     * Constructor of the component.
     *
     * @inheritdoc
     */
    constructor(props: Props) {
        super(props);

        this._onCancel = this._onCancel.bind(this);
        this._onClickMute = this._onClickMute.bind(this);
        this._onClickUnMute = this._onClickUnMute.bind(this);
    }

    /**
     * Implements {@code Component#render}.
     *
     * @inheritdoc
     */
    render() {
        const { _disableKick, _disableRemoteMute, participant, _audioMuted } = this.props;
        const buttonProps = {
            afterClick: this._onCancel,
            showLabel: true,
            participantID: participant.id,
            styles: this.props._bottomSheetStyles.buttons
        };

        const buttons = [];

        buttons.push(<MuteButton { ...buttonProps } />);

        if (!_disableKick) {
            buttons.push(<KickButton { ...buttonProps } />);
        }

        buttons.push(<PinButton { ...buttonProps } />);

        return (
            <BottomSheet onCancel = { this._onCancel }>
                <View style = { styles.participantNameContainer }>
                    <Avatar
                        participantId = { participant.id }
                        size = { AVATAR_SIZE } />
                    <Text style = { styles.participantNameLabel }>
                        { this.props._participantDisplayName }
                    </Text>
                </View>
                <TouchableOpacity onPress={_audioMuted ? this._onClickUnMute : this._onClickMute}>
                    <View
                        style = {{
                            flexDirection: 'row',
                            alignItems: 'center',
                            marginVertical: 8
                        }}>
                        <SvgUri
                            width="35"
                            height="35"
                            source={_audioMuted ? {uri:'https://svgshare.com/i/Qgo.svg'} : {uri:'https://svgshare.com/i/QgH.svg'}}
                        />
                        <Text
                            style = {{
                                marginStart: 32,
                                alignSelf: 'center'
                            }}>
                            {_audioMuted ? 'Muted' : 'Mute'}
                        </Text>
                    </View>
                </TouchableOpacity>

                <TouchableOpacity>
                    <View
                        style = {{
                            flexDirection: 'row',
                            alignItems: 'center',
                            marginVertical: 8
                        }}>
                        <SvgUri
                            width="35"
                            height="35"
                            source={_audioMuted ? {uri:'https://svgshare.com/i/Qgo.svg'} : {uri:'https://svgshare.com/i/QgH.svg'}}
                        />
                        <Text
                            style = {{
                                marginStart: 32,
                                alignSelf: 'center'
                            }}>
                            Make a presenter
                        </Text>
                    </View>
                </TouchableOpacity>
                {/* { buttons } */}
            </BottomSheet>
        );
    }

    _onClickMute(){
        const { participant, dispatch, _conferenceName } = this.props;
        const name = participant.name.split(' ').join('').replace(/[\.,-\/#!$%\^&\*;:{}=\-_`~()]/gi, '');
        const jsonRoom = {
            'type':'audioStatus',
            'user':name,
            'status':true,
            'room':_conferenceName
        };

        socket.emit('bridge', jsonRoom);
        
        console.log('mute target name: '+name);
        dispatch(muteRemote(participant.id));
    }

    _onClickUnMute(){
        const { participant, dispatch, _conferenceName } = this.props;

        const name = participant.name.split(' ').join('').replace(/[\.,-\/#!$%\^&\*;:{}=\-_`~()]/gi, '');
        const jsonRoom = {
            'type':'audioStatus',
            'user':name,
            'status':true,
            'room':_conferenceName
        };

        socket.emit('bridge', jsonRoom);
        // dispatch(unMuteRemote(participant.id));
    }

    _onClickPresenterOn(){
        const { participant, dispatch, _conferenceName } = this.props;
        const name = participant.name.split(' ').join('').replace(/[\.,-\/#!$%\^&\*;:{}=\-_`~()]/gi, '');
        const jsonRoom = {
            'type':'presenter',
            'to':name,
            'level':'siswa',
            'status':true,
            'room':_conferenceName
        };

        socket.emit('bridge', jsonRoom);
        
        console.log('mute target name: '+name);
        // dispatch(muteRemote(participant.id));
    }

    _onClickPresenterOff(){
        const { participant, dispatch, _conferenceName } = this.props;

        const name = participant.name.split(' ').join('').replace(/[\.,-\/#!$%\^&\*;:{}=\-_`~()]/gi, '');
        const jsonRoom = {
            'type':'audioStatus',
            'user':name,
            'status':true,
            'room':_conferenceName
        };

        socket.emit('bridge', jsonRoom);
        // dispatch(unMuteRemote(participant.id));
    }

    _onCancel: () => boolean;

    /**
     * Callback to hide the {@code RemoteVideoMenu}.
     *
     * @private
     * @returns {boolean}
     */
    _onCancel() {
        if (this.props._isOpen) {
            this.props.dispatch(hideRemoteVideoMenu());

            return true;
        }

        return false;
    }
}

/**
 * Function that maps parts of Redux state tree into component props.
 *
 * @param {Object} state - Redux state.
 * @param {Object} ownProps - Properties of component.
 * @private
 * @returns {Props}
 */
function _mapStateToProps(state, ownProps) {
    const { participant } = ownProps;
    const tracks = state['features/base/tracks'];
    const { remoteVideoMenu = {}, disableRemoteMute } = state['features/base/config'];
    const { disableKick } = remoteVideoMenu;
    const id = participant.id;
    const audioTrack = getTrackByMediaTypeAndParticipant(tracks, MEDIA_TYPE.AUDIO, id);
    const conferenceName = getConferenceName();

    return {
        _bottomSheetStyles: ColorSchemeRegistry.get(state, 'BottomSheet'),
        _disableKick: Boolean(disableKick),
        _disableRemoteMute: Boolean(disableRemoteMute),
        _isOpen: isDialogOpen(state, RemoteVideoMenu_),
        _participantDisplayName: getParticipantDisplayName(state, participant.id),
        _audioMuted: audioTrack?.muted ?? true,
        _conferenceName: conferenceName,
    };
}

RemoteVideoMenu_ = connect(_mapStateToProps)(RemoteVideoMenu);

export default RemoteVideoMenu_;
