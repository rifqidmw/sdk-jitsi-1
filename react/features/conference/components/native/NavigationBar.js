// @flow

import React, { Component } from 'react';
import { SafeAreaView, Text, View, Image, StyleSheet } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';

import { getConferenceName } from '../../../base/conference';
import { IconAudioOn, IconAudioOff, IconFistHand } from '../../../base/icons';
import { getFeatureFlag, MEETING_NAME_ENABLED } from '../../../base/flags';
import { connect } from '../../../base/redux';
import { PictureInPictureButton } from '../../../mobile/picture-in-picture';
import AudioMuteButton from '../../../toolbox/components/AudioMuteButton';
import { isToolboxVisible } from '../../../toolbox';
import ConferenceTimer from '../ConferenceTimer';
import { ParticipantView, getAllParticipants, PARTICIPANT_ROLE } from '../../../base/participants';
import SvgUri from 'react-native-svg-uri';
import { isLocalTrackMuted } from '../../../base/tracks';
import { MEDIA_TYPE } from '../../../base/media';
import styles, { NAVBAR_GRADIENT_COLORS } from './styles';

const styleImage = StyleSheet.create({
    container: {
      paddingTop: 50,
    },
    tinyLogo: {
      width: 50,
      height: 50,
    },
    logo: {
      width: 66,
      height: 58,
    },
  });

type Props = {

    /**
     * Name of the meeting we're currently in.
     */
    _meetingName: string,

    /**
     * Whether displaying the current meeting name is enabled or not.
     */
    _meetingNameEnabled: boolean,

    _allParticipant: Array,

    _participantId: String,

    /**
     * True if the navigation bar should be visible.
     */
    _visible: boolean,
    
    _audioMuted: boolean,
};

/**
 * Implements a navigation bar component that is rendered on top of the
 * conference screen.
 */
class NavigationBar extends Component<Props> {
    /**
     * Implements {@Component#render}.
     *
     * @inheritdoc
     */
    render() {
        const { 
            _styles,
            _allParticipant,
            _participantId,
            _audioMuted
         } = this.props;
        if (!this.props._visible) {
            return null;
        }
        return [
            <View
                pointerEvents = 'box-none'
                style = { styles.navBarWrapper }>
                <View
                    pointerEvents = 'box-none'
                    style = { styles.roomNameWrapper }>
                    {
                        this.props._meetingNameEnabled
                        && <Text
                            numberOfLines = { 1 }
                            style = { styles.roomName }>
                            { this.props._meetingName.split(' ')[2] +' '+this.props._meetingName.split(' ')[3] }
                            { console.log('meeting name full: '+this.props._meetingName)}
                        </Text>
                    }
                </View>
                <View
                    pointerEvents = 'box-none'
                    style = { styles.stopWatchWrapper }>
                    <ConferenceTimer />
                </View>
                <View
                    style = { styles.audioWrapper }>
                        {_audioMuted ? <SvgUri
                            width="20"
                            height="20"
                            source={{uri:'https://svgshare.com/i/Qkc.svg'}}
                        /> : <SvgUri
                        width="20"
                        height="20"
                        source={{uri:'https://svgshare.com/i/Pyn.svg'}}
                        />}
                    {/* <AudioMuteButton
                        styles = { styles.buttonStylesBorderless }/> */}
                </View>
            </View>
        ];
    }

}

/**
 * Maps part of the Redux store to the props of this component.
 *
 * @param {Object} state - The Redux state.
 * @returns {Props}
 */
function _mapStateToProps(state) {
    const allParticipant = getAllParticipants(state);
    const participantId = state['features/large-video'].participantId;
    const _audioMuted = isLocalTrackMuted(state['features/base/tracks'], MEDIA_TYPE.AUDIO);

    return {
        _meetingName: getConferenceName(state),
        _meetingNameEnabled: getFeatureFlag(state, MEETING_NAME_ENABLED, true),
        _visible: isToolboxVisible(state),
        _allParticipant: allParticipant,
        _participantId : participantId,
        _audioMuted,
    };
}

export default connect(_mapStateToProps)(NavigationBar);
