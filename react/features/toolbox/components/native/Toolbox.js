// @flow

import React, { PureComponent } from 'react';
import { View } from 'react-native';

import { ColorSchemeRegistry } from '../../../base/color-scheme';
import { Container } from '../../../base/react';
import { connect } from '../../../base/redux';
import { StyleType } from '../../../base/styles';
import { ChatButton } from '../../../chat';
import { Conference, plusSlides } from '../../../conference';
import { isToolboxVisible } from '../../functions';
import AudioMuteButton from '../AudioMuteButton';
import HangupButton from '../HangupButton';
import VideoMuteButton from '../VideoMuteButton';
import RaiseHandButton from './RaiseHandButton';
import OverflowMenuButton from './OverflowMenuButton';
import { ParticipantView, getAllParticipants, PARTICIPANT_ROLE } from '../../../base/participants';
import styles from './styles';
import {SwipeablePanel} from 'rn-swipeable-panel';
import SvgUri from 'react-native-svg-uri';


/**
 * The type of {@link Toolbox}'s React {@code Component} props.
 */
type Props = {

    /**
     * The color-schemed stylesheet of the feature.
     */
    _styles: StyleType,

    /**
     * The indicator which determines whether the toolbox is visible.
     */
    _visible: boolean,

    _allParticipant: Array,

    _participantId: String,

    /**
     * The redux {@code dispatch} function.
     */
    dispatch: Function
};

/**
 * Implements the conference toolbox on React Native.
 */
class Toolbox extends PureComponent<Props> {
    constructor(props) {
        super(props);

        this.state = {
            isOpen: false,
            swipeablePanelActive: false
        };
    }
    /**
     * Implements React's {@link Component#render()}.
     *
     * @inheritdoc
     * @returns {ReactElement}
     */
    render() {
        return (
            <Container
                style = { styles.toolbox }
                visible = { this.props._visible }>
                { this._renderToolbar() }
            </Container>
        );
    }

    /**
     * Constructs the toggled style of the chat button. This cannot be done by
     * simple style inheritance due to the size calculation done in this
     * component.
     *
     * @param {Object} baseStyle - The base style that was originally
     * calculated.
     * @returns {Object | Array}
     */
    _getChatButtonToggledStyle(baseStyle) {
        const { _styles } = this.props;

        if (Array.isArray(baseStyle.style)) {
            return {
                ...baseStyle,
                style: [
                    ...baseStyle.style,
                    _styles.chatButtonOverride.toggled
                ]
            };
        }

        return {
            ...baseStyle,
            style: [
                baseStyle.style,
                _styles.chatButtonOverride.toggled
            ]
        };
    }


    componentDidMount = () => {
        this.openPanel();
    };

    openPanel = () => {
        this.setState({ 
            isOpen: true,
        });
    };

    closePanel = () => {
        this.setState({ 
            isOpen:  false,
        });
    };

    /**
     * Renders the toolbar. In order to avoid a weird visual effect in which the
     * toolbar is (visually) rendered and then visibly changes its size, it is
     * rendered only after we've figured out the width available to the toolbar.
     *
     * @returns {React$Node}
     */
    _renderToolbar() {
        const { 
            _styles,
            _allParticipant,
            _participantId
         } = this.props;
        const { buttonStyles, buttonStylesBorderless, hangupButtonStyles, toggledButtonStyles } = _styles;

        for (const participant of _allParticipant) {
            if (participant.local === true){
                if (participant.role === PARTICIPANT_ROLE.MODERATOR) {
                    return (
                        // <View
                        //     accessibilityRole = 'toolbar'
                        //     pointerEvents = 'box-none'
                        //     style = { styles.toolbar }>
                        //     <AudioMuteButton
                        //         styles = { buttonStyles }
                        //         toggledStyles = { toggledButtonStyles } />
                        //     <HangupButton
                        //         styles = { hangupButtonStyles } />
                        //     <VideoMuteButton
                        //         styles = { buttonStyles }
                        //         toggledStyles = { toggledButtonStyles } />
                        // </View>
                        <View
                                accessibilityRole = 'toolbar'
                                pointerEvents = 'box-none'
                                style = { styles.toolbarParticipant }>
                                <RaiseHandButton 
                                    styles = { buttonStyles }
                                    toggledStyles = { toggledButtonStyles }
                                />
                                <HangupButton
                                    styles = { hangupButtonStyles } />
                                <SvgUri
                                    onPress={(e) => this.isOpen ? this.openPanel(): this.closePanel()}
                                    width="20"
                                    height="20"
                                    source={this.isOpen ? {uri:'https://svgshare.com/i/Pyn.svg'} : {uri:'https://svgshare.com/i/Q68.svg'}}
                                />
                            </View>
                        
                    );
                } else if (participant.role === PARTICIPANT_ROLE.NONE){
                    return (
                        <View
                            accessibilityRole = 'toolbar'
                            pointerEvents = 'box-none'
                            style = { styles.toolbarParticipant }>
                            
                        </View>
                    );
                } else if (participant.role === PARTICIPANT_ROLE.PARTICIPANT){
                    return (
                        <View
                            accessibilityRole = 'toolbar'
                            pointerEvents = 'box-none'
                            style = { styles.toolbarParticipant }>
                            <RaiseHandButton 
                                styles = { buttonStyles }
                                toggledStyles = { toggledButtonStyles }
                            />
                        </View>
                    );
                }
                break;
            }
        }
    }
}

/**
 * Maps parts of the redux state to {@link Toolbox} (React {@code Component})
 * props.
 *
 * @param {Object} state - The redux state of which parts are to be mapped to
 * {@code Toolbox} props.
 * @private
 * @returns {Props}
 */
function _mapStateToProps(state: Object): Object {
    const allParticipant = getAllParticipants(state);
    const participantId = state['features/large-video'].participantId;

    return {
        _styles: ColorSchemeRegistry.get(state, 'Toolbox'),
        _allParticipant: allParticipant,
        _participantId : participantId,
        _visible: isToolboxVisible(state)
    };
}

export default connect(_mapStateToProps)(Toolbox);
