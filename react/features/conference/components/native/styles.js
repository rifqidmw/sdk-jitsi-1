import { ColorSchemeRegistry, schemeColor } from '../../../base/color-scheme';
import { BoxModel, ColorPalette, fixAndroidViewClipping } from '../../../base/styles';
import { FILMSTRIP_SIZE } from '../../../filmstrip';

export const NAVBAR_GRADIENT_COLORS = [ '#000000FF', '#00000000' ];
export const INSECURE_ROOM_NAME_LABEL_COLOR = ColorPalette.warning;

// From brand guideline
const BOTTOM_GRADIENT_HEIGHT = 290;
const DEFAULT_GRADIENT_SIZE = 140;
const BUTTON_SIZE = 30;

/**
 * The styles of the feature conference.
 */
const toolbarButton = {
    backgroundColor: schemeColor('button'),
    borderRadius: BUTTON_SIZE / 2,
    borderWidth: 0,
    flex: 0,
    flexDirection: 'row',
    height: BUTTON_SIZE,
    justifyContent: 'center',
    marginHorizontal: 7,
    width: BUTTON_SIZE
};

export default {
    
    bottomGradient: {
        bottom: 0,
        flexDirection: 'column',
        justifyContent: 'flex-end',
        minHeight: DEFAULT_GRADIENT_SIZE,
        left: 0,
        position: 'absolute',
        right: 0
    },

    bottomSheetWrapper:{
        width                   : 50,
        height                  : 5,
        borderRadius            : 2.5,
        backgroundColor         : "#000000"
    },

    toolboxWrapper: {
        alignItems: 'flex-start',
        flexDirection: 'row',
        flexGrow: 0,
        justifyContent: 'space-between',
        marginBottom: BoxModel.margin,
        paddingHorizontal: BoxModel.margin
    },

    listWrapper: {
        bottom: 0,
        width: '100%',
        alignItems: 'flex-end',
        backgroundColor: '#fff',
    },

    tabLayoutWrapper: {
        marginBottom: 0,
      },
    /**
     * {@code Conference} style.
     */
    conference: fixAndroidViewClipping({
        alignSelf: 'stretch',
        backgroundColor: ColorPalette.appBackground,
        flex: 1
    }),

    welcome: {
        fontSize: 20,
        textAlign: 'center',
        marginTop: -5
      },
      instructions: {
        textAlign: 'center',
        color: '#333333',
        marginBottom: 5
      },

    gradient: {
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
        flex: 1
    },

    gradientStretchBottom: {
        height: BOTTOM_GRADIENT_HEIGHT
    },

    gradientStretchTop: {
        height: DEFAULT_GRADIENT_SIZE
    },

    /**
     * View that contains the indicators.
     */
    indicatorContainer: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'flex-end',
        margin: BoxModel.margin
    },

    /**
     * Indicator container for wide aspect ratio.
     */
    indicatorContainerWide: {
        marginRight: FILMSTRIP_SIZE + BoxModel.margin
    },

    labelWrapper: {
        flexDirection: 'column',
        position: 'absolute',
        right: 0,
        top: 0
    },

    lonelyButton: {
        alignItems: 'center',
        borderRadius: 24,
        flexDirection: 'row',
        height: 48,
        justifyContent: 'space-around',
        paddingHorizontal: 12
    },

    lonelyButtonComponents: {
        marginHorizontal: 6
    },

    lonelyMeetingContainer: {
        alignSelf: 'stretch',
        alignItems: 'center',
        padding: BoxModel.padding * 2
    },

    lonelyMessage: {
        paddingVertical: 12
    },

    navBarButton: {
        iconStyle: {
            color: ColorPalette.white,
            fontSize: 24
        },

        underlayColor: 'transparent'
    },

    navBarContainer: {
        flexDirection: 'column',
        left: 0,
        position: 'absolute',
        right: 0,
        top: 0
    },

    navBarSafeView: {
        left: 0,
        position: 'absolute',
        right: 0,
        top: 0
    },

    navBarWrapper: {
        alignItems: 'center',
        flex: 1,
        flexDirection: 'row',
        height: 44,
        justifyContent: 'space-between',
        paddingHorizontal: 14
    },

    navBarWrapper: {
        alignItems: 'center',
        flex: 1,
        flexDirection: 'row',
        height: 44,
        justifyContent: 'space-between',
        paddingHorizontal: 14
    },

    participantItem:{
        flexDirection: 'column',
        alignItems: 'flex-start',
        backgroundColor: '#fff',
        justifyContent: 'center',
    },

    participantText: {
        color: ColorPalette.white,
        fontSize: 15,
        opacity: 0.6
    },

    roomTimer: {
        color: ColorPalette.white,
        fontSize: 15,
        opacity: 0.6
    },

    roomName: {
        color: ColorPalette.white,
        fontSize: 12,
        fontWeight: '400'
    },

    roomNameWrapper: {
        alignItems: 'flex-start',
        left: 0,
        right: 0
    },

    stopWatchWrapper: {
        alignItems: 'center',
        left: 0,
        right: 0
    },

    audioWrapper: {
        alignItems: 'flex-end',
        left: 0,
        right: 0,
        marginTop: 12
    },

    bottomWrapper: {
        flexDirection: 'column',
        bottom: 0,
        width: '100%',
        alignItems: 'flex-end',
        backgroundColor: '#fff',
        justifyContent: 'center'
    },


    //chat style
    flatListWrapper: {
        flex: 1,
        backgroundColor: 'transparent'
    },

    containerMyMessageStyle: {
        alignSelf: 'flex-end',
        flexDirection: 'row',
        marginHorizontal: 12,
        marginTop: 4
    },

    containerTheirMessageStyle: {
        alignSelf: 'flex-start',
        flexDirection: 'row',
        marginHorizontal: 12,
        marginTop: 4
    },

    myMessageStyle: {
        margin: 10,
        padding: 10,
        backgroundColor: '#F2F4F7',
        borderColor: '#979797',
        borderStyle: 'solid',
        borderWidth: 1,
        borderRadius: 10,
        alignSelf: 'flex-end'
    },
    theirMessageStyle: {
        margin: 10,
        padding: 10,
        backgroundColor: '#F2F4F7',
        borderColor: '#979797',
        borderStyle: 'solid',
        borderWidth: 1,
        borderRadius: 10,
        alignSelf: 'flex-start'
    },

    sendButtonIcon: {
        color: ColorPalette.darkGrey,
        fontSize: 22
    },

    inputField: {
        color: 'rgb(28, 32, 37)',
        flex: 1,
        height: 48
    },
    /**
     * The style of the {@link View} which expands over the whole
     * {@link Conference} area and splits it between the {@link Filmstrip} and
     * the {@link Toolbox}.
     */
    toolboxAndFilmstripContainer: {
        bottom: 0,
        flexDirection: 'column',
        justifyContent: 'flex-end',
        left: 0,
        paddingBottom: BoxModel.padding,
        position: 'absolute',
        right: 0,

        // Both on Android and iOS there is the status bar which may be visible.
        // On iPhone X there is the notch. In the two cases BoxModel.margin is
        // not enough.
        top: BoxModel.margin * 3
    },

    insecureRoomNameLabel: {
        backgroundColor: INSECURE_ROOM_NAME_LABEL_COLOR
    },

    buttonStylesBorderless: {
        style: {
            ...toolbarButton,
            backgroundColor: 'transparent',
            alignItems: 'flex-start',
            width: BUTTON_SIZE,
            height: BUTTON_SIZE
        }
    }
};

ColorSchemeRegistry.register('Conference', {
    lonelyButton: {
        backgroundColor: schemeColor('inviteButtonBackground')
    },

    lonelyMessage: {
        color: schemeColor('onVideoText')
    }
});
