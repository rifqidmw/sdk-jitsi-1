import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  Image,
  TextInput,
  TouchableHighlight,
  Dimensions,
  AlertIOS

} from 'react-native';


import io from 'socket.io-client';
import { GiftedChat } from 'react-native-gifted-chat';
import { getConferenceName } from '../../../base/conference';


var width = Dimensions.get('window').width;
var height = Dimensions.get('window').height;
export default class NewChat extends Component {

   constructor(props) {
    super(props);
    this.socket = this.props.socket
    this.state = {
      
      message:"",
      messages: []
     

    };
  }


 
componentDidMount = () => {
    this.startSocket();
};

startSocket(){
    const socket = io('https://bridge.palapabeta.com/', {      
        transports: ['websocket', 'polling']});   
    socket.io.connect();

    // this.onHandleMessageSocket(socket);
}

onHandleMessageSocket(socket){
    console.log('onHandleMessageSocket is open');
    socket.on('connect', () => { 
        console.log('socket react connected to server'); 
    }); 
    socket.on('disconnect', () => { 
        console.log('socket react disconnect to server'); 
    }); 
    socket.on('reconnect', () => { 
        console.log('socket react reconnect to server'); 
    }); 

    const jsonRoom = {
        'room': '20200817 DEMO-SEKOLAH DEMO-IPA',
      };

    socket.emit('room', jsonRoom)

    console.log('send socket room: '+ jsonRoom)

    const jsonBridge = {
        'type': 'cekRome',
        'room': getConferenceName(),
      };
    
    socket.emit('bridge', jsonBridge)
    console.log('send socket bridge: '+ jsonBridge)

    socket.on('bridge', this.onReceivedMessage); 
}

onReceivedMessage(message) {
    if (message['type'] === 'chat'){
        console.log(message);

        var data = [ {  type: message['type'],
                        data: message['data'],
                        from: message['from'],
                        room: message['room'] } ]


        this.setState((previousState) => {
            return {
            messages: GiftedChat.append(previousState.messages, data),
            };
        });

        console.log('data from array: '+this.state.messages);

    }
}

   
  _renderChat(chat){

      return chat.map(function(data, i){
        return(
          <View key={i} style={styles.msj}>
            <Text style={styles.user}>{data.message}</Text>
          </View>
        );
      });

  }
  _goBack(){
    this.props.navigator.pop()
  }

  render() {

    

    return (

  <View style={styles.chatWrap}>

     <GiftedChat
        style={{borderWidth:1}}
        messages={this.state.messages}
        user={{
          _id: 1,
         
        }}
      />
  </View>
     

    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#FFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
   
  },
  form:{
    borderWidth:1,
    flexDirection:'row'
  },
  input: {
    flex: 1,
    paddingHorizontal: 10,
    color:'#444',
    backgroundColor:'#FFF',
    borderRadius:15,
    borderWidth:1,
    borderColor:'#dfdfdf'
  },
  inputWrap: {
    padding:5,
    flexDirection: "row",
    borderWidth:1,
    borderBottomColor:'#f2f2f2',
    borderLeftColor:'#f2f2f2',
    borderRightColor:'#f2f2f2',
    borderTopColor:'#c3c3c3',
    height: 40,
    borderBottomWidth: 1,
    borderBottomColor: "#CCC",
    backgroundColor:'#fdfdfd'
  },
  iconWrap: {
    paddingHorizontal: 7,
    alignItems: "center",
    justifyContent: "center",
  },
  icon: {
    height: 20,
    width: 20,
  },
  chatWrap:{
    width:width,
    height:height
  },
  topBar:{
    marginTop:20,
    borderWidth:1
  },
  msj:{
    borderWidth:1,
    alignItems:'flex-end'
  }
});